\begin{algorithm}
Zvoliť počiatočné hodnoty pre parametre: $a_0$, $c_0$\;
\For{$k = 1 \text{ až } pocet\_krokov$}{

$\Delta_a \leftarrow 0$; $\Delta_c \leftarrow 0$\;

\ForEach{$(x, d) \in P$}{
	$\Delta_a \leftarrow \Delta_a - \gamma
	\frac{\partial E(x, d, a_{k-1}, c_{k-1})}{\partial a}$\;
	$\Delta_c \leftarrow \Delta_c - \gamma
	\frac{\partial E(x, d, a_{k-1}, c_{k-1})}{\partial c}$\;
}

$a_{k} = a_{k-1} + \Delta_a$\;
$c_{k} = c_{k-1} + \Delta_c$\;
}
\caption{Príklad pseudokódu.}
\label{alg:graddesc_iterative}
\end{algorithm}
