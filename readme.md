This repository will include materials I used when writing my master thesis.

Requirements can be installed with:
pip3 install -r requirements.txt

Notes: 
	Install (or make sure it is installed) swig and cmake.

	git should be installed on your system of choice and part of the final logging
	requires that the current git repo hash be read and written into a file if this
	is not behaviour you need check settings.py - save_default_settings(...) and modify
	it, to remove this.

	If you at this point haven't installed lambda stack install pytorch.
